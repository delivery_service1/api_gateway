package handler

import (
	"api_gateway/api/models"
	"api_gateway/config"
	"api_gateway/grpc/client"
	"fmt"

	"github.com/gin-gonic/gin"
)

type Handler struct {
	cfg      config.Config
	services client.IServiceManager
}

func New(cfg config.Config,services client.IServiceManager) Handler {
	return Handler{
		cfg:      cfg,
		services: services,
	}
}

func handleResponse(c *gin.Context, msg string, statusCode int, data interface{}) {
	resp := models.Response{}

	switch code := statusCode; {
	case code < 400:
		resp.Description = "OK"
		fmt.Println("~~~~> OK", "msg", msg,"status", code)
	case code == 401:
		resp.Description = "Unauthorized"
	case code < 500:
		resp.Description = "Bad Request"
		fmt.Println("!!!!! BAD REQUEST",  "msg", msg,"status", code)
	default:
		resp.Description = "Internal Server Error"
		fmt.Println("!!!!! INTERNAL SERVER ERROR", "msg", msg,"status", code,"error", data)
	}

	resp.StatusCode = statusCode
	resp.Data = data

	c.JSON(resp.StatusCode, resp)
}
