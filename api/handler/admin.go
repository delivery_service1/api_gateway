package handler

import (
	pbu "api_gateway/genproto/user_protos"
	"fmt"

	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateAdmin godoc
// @Router       /admin [POST]
// @Summary      Create a new admin
// @Description  Create a new admin
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param        admin body models.CreateAdmin false "admin"
// @Success      201  {object}  models.Admin
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateAdmin(c *gin.Context) {
	request := pbu.CreateAdminRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.AdminService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating admin", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "admin Created!", http.StatusCreated, resp)
}

// GetAdmin godoc
// @Router       /admin/{id} [GET]
// @Summary      Gets admin
// @Description  get admin by id
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param        id path string true "admin"
// @Success      200  {object}  models.Admin
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAdmin(c *gin.Context) {
	var err error

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	admin, err := h.services.AdminService().Get(ctx, &pbu.AdminPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting admin by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, admin)
}

// GetAdminList godoc
// @Router       /admins [GET]
// @Summary      Get admin list
// @Description  get admin list
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.AdminsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAdminList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.AdminService().GetList(ctx, &pbu.GetAdminListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting admins", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateAdmin godoc
// @Router       /admin/{id} [PUT]
// @Summary      Update admin
// @Description  update admin
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param 		 id path string true "admin_id"
// @Param        admin body models.UpdateAdmin true "admin"
// @Success      200  {object}  models.Admin
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateAdmin(c *gin.Context) {
	updateAdmin := pbu.UpdateAdmin{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	if err := c.ShouldBindJSON(&updateAdmin); err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateAdmin.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.AdminService().Update(ctx, &updateAdmin)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating admin", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteAdmin godoc
// @Router       /admin/{id} [DELETE]
// @Summary      Delete admin
// @Description  delete admin
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param 		 id path string true "admin_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteAdmin(c *gin.Context) {
	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	empty, err := h.services.AdminService().Delete(ctx, &pbu.AdminPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting admin!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, empty)
}


// UpdateAdminPassword godoc
// @Router       /admin_password [PUT]
// @Summary      Update admin_password
// @Description  update admin_password
// @Tags         admin
// @Accept       json
// @Produce      json
// @Param        admin body models.ChangeAdminPasswordRequest true "admin"
// @Success      200  {object}  models.AdminHashedPasswordResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateAdminPassword(c *gin.Context)  {
	request := pbu.ChangeAdminPasswordRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil{
		handleResponse(c,"Error while reading body forom client!", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	fmt.Println(request.Login)
	fmt.Println(request.OldPassword)
	fmt.Println(request.NewPassword)
	resp, err := h.services.AdminService().UpdatePassword(ctx, &request)
	if err != nil{
		handleResponse(c,"Error while updating password!", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Password Successfully updated!", http.StatusOK, resp)
}