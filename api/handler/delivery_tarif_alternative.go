package handler

import (
	pbo "api_gateway/genproto/order_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateDeliveryTarifAlternative godoc
// @Router       /delivery_tarif_alternative [POST]
// @Summary      Create a new delivery_tarif_alternative
// @Description  Create a new delivery_tarif_alternative
// @Tags         delivery_tarif_alternative
// @Accept       json
// @Produce      json
// @Param        delivery_tarif_alternative body models.CreateDeliveryTarifAlternative false "delivery_tarif_alternative"
// @Success      201  {object}  models.DeliveryTarifAlternative
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateDeliveryTarifAlternative(c *gin.Context) {
	request := pbo.CreateDeliveryTarifAlternativeRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.DeliveryTarifAlternativeService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating delivery_tarif_alternative", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "delivery_tarif_alternative Created!", http.StatusCreated, resp)
}

// GetDeliveryTarifAlternative godoc
// @Router       /delivery_tarif_alternative/{id} [GET]
// @Summary      Gets delivery_tarif_alternative
// @Description  get delivery_tarif_alternative by id
// @Tags         delivery_tarif_alternative
// @Accept       json
// @Produce      json
// @Param        id path string true "delivery_tarif_alternative"
// @Success      200  {object}  models.DeliveryTarifAlternative
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTarifAlternative(c *gin.Context) {
	var err error

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	DeliveryTarif, err := h.services.DeliveryTarifAlternativeService().Get(ctx, &pbo.DeliveryTarifAlternativePrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting delivery_tarif_alternative by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, DeliveryTarif)
}

// GetDeliveryTarifAlternativeList godoc
// @Router       /delivery_tarif_alternatives [GET]
// @Summary      Get delivery_tarif_alternative list
// @Description  get delivery_tarif_alternative list
// @Tags         delivery_tarif_alternative
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.DeliveryTarifAlternativesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTarifAlternativeList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.DeliveryTarifAlternativeService().GetList(ctx, &pbo.GetDeliveryTarifAlternativeListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting delivery_tarif_alternatives", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateDeliveryTarifAlternative godoc
// @Router       /delivery_tarif_alternative/{id} [PUT]
// @Summary      Update delivery_tarif_alternative
// @Description  update delivery_tarif_alternative
// @Tags         delivery_tarif_alternative
// @Accept       json
// @Produce      json
// @Param 		 id path string true "delivery_tarif_alternative_id"
// @Param        delivery_tarif_alternative body models.UpdateDeliveryTarifAlternative true "delivery_tarif_alternative"
// @Success      200  {object}  models.DeliveryTarifAlternative
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateDeliveryTarifAlternative(c *gin.Context) {
	updateDeliveryTarifAlternative := pbo.UpdateDeliveryTarifAlternative{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	if err := c.ShouldBindJSON(&updateDeliveryTarifAlternative); err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateDeliveryTarifAlternative.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.DeliveryTarifAlternativeService().Update(ctx, &updateDeliveryTarifAlternative)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating delivery_tarif_alternative", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteDeliveryTarif godoc
// @Router       /delivery_tarif_alternative/{id} [DELETE]
// @Summary      Delete delivery_tarif_alternative
// @Description  delete delivery_tarif_alternative
// @Tags         delivery_tarif_alternative
// @Accept       json
// @Produce      json
// @Param 		 id path string true "delivery_tarif_alternative_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteDeliveryTarifAlternative(c *gin.Context) {
	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	empty, err := h.services.DeliveryTarifAlternativeService().Delete(ctx, &pbo.DeliveryTarifAlternativePrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting delivery_tarif_alternative!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, empty)
}
