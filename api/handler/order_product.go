package handler

import (
	pbo "api_gateway/genproto/order_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateOrderProduct godoc
// @Router       /order_product [POST]
// @Summary      Create a new order_product
// @Description  Create a new order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        order_product body models.CreateOrderProduct false "order_product"
// @Success      201  {object}  models.OrderProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateOrderProduct(c *gin.Context) {
	request := pbo.CreateOrderProductRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.OrderProductService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating order_product", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "order_product Created!", http.StatusCreated, resp)
}

// GetOrderProduct godoc
// @Router       /order_product/{id} [GET]
// @Summary      Gets order_product
// @Description  get order_product by id
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        id path string true "order_product"
// @Success      200  {object}  models.OrderProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderProduct(c *gin.Context) {
	var err error

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	orderProduct, err := h.services.OrderProductService().Get(ctx, &pbo.OrderProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting order_product by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, orderProduct)
}

// GetOrderProductList godoc
// @Router       /order_products [GET]
// @Summary      Get order_product list
// @Description  get order_product list
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.OrderProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderProductList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.OrderProductService().GetList(ctx, &pbo.GetOrderProductListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting order_products", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateOrderProduct godoc
// @Router       /order_product/{id} [PUT]
// @Summary      Update order_product
// @Description  update order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "order_product_id"
// @Param        order_product body models.UpdateOrderProduct true "order_product"
// @Success      200  {object}  models.OrderProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrderProduct(c *gin.Context) {
	updateOrderProduct := pbo.UpdateOrderProduct{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	if err := c.ShouldBindJSON(&updateOrderProduct); err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateOrderProduct.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.OrderProductService().Update(ctx, &updateOrderProduct)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating order_product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteOrderProduct godoc
// @Router       /order_product/{id} [DELETE]
// @Summary      Delete order_product
// @Description  delete order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "order_product_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteOrderProduct(c *gin.Context) {
	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	empty, err := h.services.OrderProductService().Delete(ctx, &pbo.OrderProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting order_product!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, empty)
}
