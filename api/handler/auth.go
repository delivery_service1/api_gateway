package handler

import (
	"api_gateway/api/models"
	"api_gateway/config"
	pbu "api_gateway/genproto/user_protos"
	"api_gateway/pkg/jwt"
	"context"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
 )

// ClientLogin godoc
// @Router       /client_login [POST]
// @Summary      Client Login
// @Description  Client Login
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        login body models.UserLoginRequest false "login"
// @Success      201  {object}  models.UserLoginResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) ClientLogin(c *gin.Context) {
	var (
		loginRequest  = pbu.UserLoginRequest{}
		loginResponse = models.UserLoginResponse{}
	)

	if err := c.ShouldBindJSON(&loginRequest); err != nil {
		handleResponse(c, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.AuthService().ClientLogin(ctx, &loginRequest)
	if err != nil {
		handleResponse(c,  "Error while getting client credentials!", http.StatusInternalServerError, err.Error())
		return
	}

	claims := map[string]interface{}{
		"user_id": resp.GetId(),
	}

	accessToken, err := jwt.GenerateJWT(claims, config.AccessExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, "error while generating jwt", http.StatusInternalServerError, err.Error())
		return
	}

	refreshToken, err := jwt.GenerateJWT(claims, config.RefreshExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, "error while generating jwt", http.StatusInternalServerError, err.Error())
		return
	}

	loginResponse.AccessToken = accessToken
	loginResponse.RefreshToken = refreshToken

	handleResponse(c, "", http.StatusOK, loginResponse)
}

// AdminLogin godoc
// @Router       /admin_login [POST]
// @Summary      Admin Login
// @Description  Admin Login
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        login body models.UserLoginRequest false "login"
// @Success      201  {object}  models.UserLoginResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) AdminLogin(c *gin.Context) {
	var (
		loginRequest  = pbu.UserLoginRequest{}
		loginResponse = models.UserLoginResponse{}
	)

	if err := c.ShouldBindJSON(&loginRequest); err != nil {
		handleResponse(c, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.AuthService().AdminLogin(ctx, &loginRequest)
	if err != nil {
		handleResponse(c,  "Error while getting client credentials!", http.StatusInternalServerError, err.Error())
		return
	}

	claims := map[string]interface{}{
		"user_id": resp.GetId(),
	}

	accessToken, err := jwt.GenerateJWT(claims, config.AccessExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, "error while generating jwt", http.StatusInternalServerError, err.Error())
		return
	}

	refreshToken, err := jwt.GenerateJWT(claims, config.RefreshExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, "error while generating jwt", http.StatusInternalServerError, err.Error())
		return
	}

	loginResponse.AccessToken = accessToken
	loginResponse.RefreshToken = refreshToken

	handleResponse(c, "", http.StatusOK, loginResponse)
}

// CourierLogin godoc
// @Router       /courier_login [POST]
// @Summary      Courier Login
// @Description  Courier Login
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        login body models.UserLoginRequest false "login"
// @Success      201  {object}  models.UserLoginResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CourierLogin(c *gin.Context) {
	var (
		loginRequest  = pbu.UserLoginRequest{}
		loginResponse = models.UserLoginResponse{}
	)

	if err := c.ShouldBindJSON(&loginRequest); err != nil {
		handleResponse(c, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.AuthService().CourierLogin(ctx, &loginRequest)
	if err != nil {
		handleResponse(c,  "Error while getting client credentials!", http.StatusInternalServerError, err.Error())
		return
	}

	claims := map[string]interface{}{
		"user_id": resp.GetId(),
	}

	accessToken, err := jwt.GenerateJWT(claims, config.AccessExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, "error while generating jwt", http.StatusInternalServerError, err.Error())
		return
	}

	refreshToken, err := jwt.GenerateJWT(claims, config.RefreshExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, "error while generating jwt", http.StatusInternalServerError, err.Error())
		return
	}

	loginResponse.AccessToken = accessToken
	loginResponse.RefreshToken = refreshToken

	handleResponse(c, "", http.StatusOK, loginResponse)
}

func (h Handler) GetAuthInfoFromToken(c *gin.Context) (models.AuthInfo, error) {
	accessToken := c.GetHeader("Authorization")
	if accessToken == "" {
 		return models.AuthInfo{}, config.ErrUnauthorized
	}

	claims, err := jwt.ExtractClaims(accessToken, string(config.SignKey))
	if err != nil {
		return models.AuthInfo{}, err
	}

	userID := cast.ToString(claims["user_id"])

	return models.AuthInfo{
		UserID: userID,
	}, nil
}

