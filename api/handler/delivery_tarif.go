package handler

import (
	pbo "api_gateway/genproto/order_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateDeliveryTarif godoc
// @Router       /delivery_tarif [POST]
// @Summary      Create a new delivery_tarif
// @Description  Create a new delivery_tarif
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        delivery_tarif body models.CreateDeliveryTarif false "delivery_tarif"
// @Success      201  {object}  models.DeliveryTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateDeliveryTarif(c *gin.Context) {
	request := pbo.CreateDeliveryTarifRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.DeliveryTarifService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating delivery_tarif", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "delivery_tarif Created!", http.StatusCreated, resp)
}

// GetDeliveryTarif godoc
// @Router       /delivery_tarif/{id} [GET]
// @Summary      Gets delivery_tarif
// @Description  get delivery_tarif by id
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        id path string true "delivery_tarif"
// @Success      200  {object}  models.DeliveryTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTarif(c *gin.Context) {
	var err error

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	DeliveryTarif, err := h.services.DeliveryTarifService().Get(ctx, &pbo.DeliveryTarifPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting delivery_tarif by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, DeliveryTarif)
}

// GetDeliveryTarifList godoc
// @Router       /delivery_tarifs [GET]
// @Summary      Get delivery_tarif list
// @Description  get delivery_tarif list
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.DeliveryTarifsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTarifList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.DeliveryTarifService().GetList(ctx, &pbo.GetDeliveryTarifListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting delivery_tarifs", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateDeliveryTarif godoc
// @Router       /delivery_tarif/{id} [PUT]
// @Summary      Update delivery_tarif
// @Description  update delivery_tarif
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param 		 id path string true "delivery_tarif_id"
// @Param        delivery_tarif body models.UpdateDeliveryTarif true "delivery_tarif"
// @Success      200  {object}  models.DeliveryTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateDeliveryTarif(c *gin.Context) {
	updateDeliveryTarif := pbo.UpdateDeliveryTarif{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	if err := c.ShouldBindJSON(&updateDeliveryTarif); err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateDeliveryTarif.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.DeliveryTarifService().Update(ctx, &updateDeliveryTarif)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating delivery_tarif", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteDeliveryTarif godoc
// @Router       /delivery_tarif/{id} [DELETE]
// @Summary      Delete delivery_tarif
// @Description  delete delivery_tarif
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param 		 id path string true "delivery_tarif_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteDeliveryTarif(c *gin.Context) {
	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	empty, err := h.services.DeliveryTarifService().Delete(ctx, &pbo.DeliveryTarifPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting delivery_tarif!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, empty)
}
