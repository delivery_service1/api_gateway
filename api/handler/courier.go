package handler

import (
	pbc "api_gateway/genproto/user_protos"
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateCourier godoc
// @Router       /courier [POST]
// @Summary      Create a new courier
// @Description  Create a new courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        courier body models.CreateCourier false "courier"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCourier(c *gin.Context) {
	request := pbc.CreateCourierRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from courier", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.CourierService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating courier", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "courier Created!", http.StatusCreated, resp)
}

// GetCourier godoc
// @Router       /courier/{id} [GET]
// @Summary      Gets courier
// @Description  get courier by id
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier"
// @Success      200  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourier(c *gin.Context) {
	var err error

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	courier, err := h.services.CourierService().Get(ctx, &pbc.CourierPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting courier by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, courier)
}

// GetCourierList godoc
// @Router       /couriers [GET]
// @Summary      Get courier list
// @Description  get courier list
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.CouriersResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourierList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.CourierService().GetList(ctx, &pbc.GetCourierListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting courieres", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateCourier godoc
// @Router       /courier/{id} [PUT]
// @Summary      Update courier
// @Description  update courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param 		 id path string true "courier_id"
// @Param        courier body models.UpdateCourier true "courier"
// @Success      200  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCourier(c *gin.Context) {
	updateCourier := pbc.UpdateCourier{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	if err := c.ShouldBindJSON(&updateCourier); err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateCourier.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.CourierService().Update(ctx, &updateCourier)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating courier", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteCourier godoc
// @Router       /courier/{id} [DELETE]
// @Summary      Delete courier
// @Description  delete courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param 		 id path string true "courier_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCourier(c *gin.Context) {
	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	empty, err := h.services.CourierService().Delete(ctx, &pbc.CourierPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting courier!", http.StatusInternalServerError, err.Error())
		return
	}
	
	handleResponse(c, "Deleted successfully!", http.StatusOK, empty)
}

// UpdateCourierPassword godoc
// @Router       /courier_password [PUT]
// @Summary      Update courier_password
// @Description  update courier_password
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        courier body models.ChangeCourierPasswordRequest true "courier"
// @Success      200  {object}  models.CourierHashedPasswordResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCourierPassword(c *gin.Context)  {
	request := pbc.ChangeCourierPasswordRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil{
		handleResponse(c,"Error while reading body forom client!", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.CourierService().UpdatePassword(ctx, &request)
	if err != nil{
		handleResponse(c,"Error while updating password!", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Password Successfully updated!", http.StatusOK, resp)
}