package handler

import (
	pbc "api_gateway/genproto/user_protos"
	"fmt"

	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateClient godoc
// @Router       /client [POST]
// @Summary      Create a new client
// @Description  Create a new client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        client body models.CreateClient false "client"
// @Success      201  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateClient(c *gin.Context) {
	request := pbc.CreateClientRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ClientService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating client", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "client Created!", http.StatusCreated, resp)
}

// GetClient godoc
// @Router       /client/{id} [GET]
// @Summary      Gets client
// @Description  get client by id
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        id path string true "client"
// @Success      200  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClient(c *gin.Context) {
	var err error

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	client, err := h.services.ClientService().Get(ctx, &pbc.ClientPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting client by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, client)
}

// GetClientList godoc
// @Router       /clients [GET]
// @Summary      Get client list
// @Description  get client list
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.ClientsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClientList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ClientService().GetList(ctx, &pbc.GetClientListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting clientes", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateClient godoc
// @Router       /client/{id} [PUT]
// @Summary      Update client
// @Description  update client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param 		 id path string true "client_id"
// @Param        client body models.UpdateClient true "client"
// @Success      200  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateClient(c *gin.Context) {
	updateClient := pbc.UpdateClient{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	if err := c.ShouldBindJSON(&updateClient); err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateClient.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ClientService().Update(ctx, &updateClient)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating client", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteClient godoc
// @Router       /client/{id} [DELETE]
// @Summary      Delete client
// @Description  delete client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param 		 id path string true "client_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteClient(c *gin.Context) {
	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	empty, err := h.services.ClientService().Delete(ctx, &pbc.ClientPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting client!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, empty)
}


// UpdateClientPassword godoc
// @Router       /client_password [PUT]
// @Summary      Update client_password
// @Description  update client_password
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        client body models.ChangeClientPasswordRequest true "client"
// @Success      200  {object}  models.ClientHashedPasswordResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateClientPassword(c *gin.Context)  {
	request := pbc.ChangeClientPasswordRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil{
		handleResponse(c,"Error while reading body forom client!", http.StatusBadRequest, err)
		return
	}

	fmt.Println(request.Login)
	fmt.Println(request.OldPassword)
	fmt.Println(request.NewPassword)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ClientService().UpdatePassword(ctx, &request)
	if err != nil{
		handleResponse(c,"Error while updating password!", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Password Successfully updated!", http.StatusOK, resp)
}