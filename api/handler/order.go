package handler

import (
	"api_gateway/config"
	pbo "api_gateway/genproto/order_protos"
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateOrder godoc
// @Security ApiKeyAuth
// @Router       /order [POST]
// @Summary      Create a new order
// @Description  Create a new order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        order body models.CreateOrder false "order"
// @Success      201  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateOrder(c *gin.Context) {
	_, err := h.GetAuthInfoFromToken(c)
	if err != nil{
		handleResponse(c, "Error Unauthorized!",http.StatusUnauthorized, config.ErrUnauthorized)
		return
	}

	request := pbo.CreateOrderRequest{}

	err = c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.OrderService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating order", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "order created!", http.StatusCreated, resp.Id)
}

// GetOrder godoc
// @Router       /order/{id} [GET]
// @Summary      Gets order
// @Description  get order by id
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        id path string true "order"
// @Success      200  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrder(c *gin.Context) {
	var err error

	id := c.Param("id")
  
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	order, err := h.services.OrderService().Get(ctx, &pbo.OrderPrimaryKey{Id: id})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting order by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, order)
}

// GetOrderList godoc
// @Router       /orders [GET]
// @Summary      Get order list
// @Description  get order list
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.OrdersResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.OrderService().GetList(ctx, &pbo.GetOrderListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting orders", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateOrder godoc
// @Router       /order/{id} [PUT]
// @Summary      Update order
// @Description  update order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param 		 id path string true "order_id"
// @Param        order body models.UpdateOrder true "order"
// @Success      200  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrder(c *gin.Context) {
	updateOrder := pbo.UpdateOrder{}

	id := c.Param("id")
 
	if err := c.ShouldBindJSON(&updateOrder); err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateOrder.Id = id

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.OrderService().Update(ctx, &updateOrder)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating order", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteOrder godoc
// @Router       /order/{id} [DELETE]
// @Summary      Delete order
// @Description  delete order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param 		 id path string true "order_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteOrder(c *gin.Context) {
	uid := c.Param("id")
 
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	empty, err := h.services.OrderService().Delete(ctx, &pbo.OrderPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting order!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, empty)
}

// UpdateOrderStatus godoc
// @Router       /order_status [PATCH]
// @Summary      Update order status
// @Description  update order status
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        order body models.UpdateOrderStatusRequest true "order"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrderStatus(c *gin.Context) {
	updateOrderStatus := pbo.UpdateOrderStatusRequest{}

	if err := c.ShouldBindJSON(&updateOrderStatus); err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.OrderService().UpdateOrderStatus(ctx, &updateOrderStatus)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating order", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}