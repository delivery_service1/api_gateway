package handler

import (
	pbc "api_gateway/genproto/catalog_protos"
	 
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateProduct godoc
// @Router       /product [POST]
// @Summary      Create a new product
// @Description  Create a new product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        product body models.CreateProduct false "product"
// @Success      201  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateProduct(c *gin.Context) {
	request := pbc.CreateProductRequest{}

	err := c.ShouldBindJSON(&request)
	if err != nil {
		handleResponse(c, "Error in handlers, while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ProductService().Create(ctx, &request)
	if err != nil {
		handleResponse(c,"Error in handlers, while creating product", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Product Created!", http.StatusCreated, resp)
}

// GetProduct godoc
// @Router       /product/{id} [GET]
// @Summary      Gets product
// @Description  get product by id
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        id path string true "product"
// @Success      200  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProduct(c *gin.Context) {
	var err error

	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	
	product, err := h.services.ProductService().Get(ctx, &pbc.ProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting product by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "", http.StatusOK, product)
}

// GetProductList godoc
// @Router       /products [GET]
// @Summary      Get product list
// @Description  get product list
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param 		 limit query string false "limit"
// @Param 		 search query string false "search"
// @Success      200  {object}  models.ProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProductList(c *gin.Context) {
	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, "Error in handlers, while parsing page into int!", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "10")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, "Error while parsing limit into int", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ProductService().GetList(ctx, &pbc.GetProductListRequest{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})
	if err != nil {
		handleResponse(c, "Error in handlers, while getting products", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, "Success!", http.StatusOK, resp)
}

// UpdateProduct godoc
// @Router       /product/{id} [PUT]
// @Summary      Update product
// @Description  update product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "product_id"
// @Param        product body models.UpdateProduct true "product"
// @Success      200  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateProduct(c *gin.Context) {
	updateProduct := &pbc.UpdateProduct{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c,"invalid uuid!", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	if err := c.ShouldBindJSON(&updateProduct); err != nil {
		handleResponse(c, "Error in handlers, while reading body", http.StatusBadRequest, err.Error())
		return
	}

	updateProduct.Id = uid

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.ProductService().Update(ctx, updateProduct)
	if err != nil {
		handleResponse(c, "Error in handlers, while updating product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Updated successfully", http.StatusOK, resp)
}

// DeleteProduct godoc
// @Router       /product/{id} [DELETE]
// @Summary      Delete product
// @Description  delete product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param 		 id path string true "product_id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteProduct(c *gin.Context) {
	uid := c.Param("id")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	empty, err := h.services.ProductService().Delete(ctx, &pbc.ProductPrimaryKey{Id: uid})
	if err != nil {
		handleResponse(c, "Error in handlers while deleting product!", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, "Deleted successfully!", http.StatusOK, empty)
}
