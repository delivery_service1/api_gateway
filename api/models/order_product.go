package models

import (
	"time"
)

type OrderProduct struct {
	ID           string`json:"id"`
	ProductID    string`json:"product_id"`
	OrderID       int32`json:"order_id"`
	Quantity      int32`json:"quantity"`
	Price       float32`json:"price"`
	CreatedAt time.Time`json:"created_at"`
	UpdatedAt time.Time`json:"updated_at"`
}

type CreateOrderProduct struct { 
	ProductID    string`json:"product_id"`
	OrderID       int32`json:"order_id"`
	Quantity      int32`json:"quantity"`
	Price       float32`json:"price"`
}

type UpdateOrderProduct struct {
	ID           string`json:"id"`
	ProductID    string`json:"product_id"`
	OrderID       int32`json:"order_id"`
	Quantity      int32`json:"quantity"`
	Price       float32`json:"price"`
}

type OrderProductsResponse struct {
	OrderProducts []OrderProduct   `json:"order_products"`
	Count			int32`json:"count"`
}

 