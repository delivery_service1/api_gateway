package models

import (
	"time"
)

type Admin struct {
	ID 	         string`json:"id"`
	FirstName    string`json:"first_name"`
	LastName     string`json:"last_name"`
	Phone        string`json:"phone"`
	Active       string`json:"active"`
	CreatedAt time.Time`json:"created_at"`
	UpdatedAt time.Time`json:"updated_at"`
}

type CreateAdmin struct {
	FirstName    string`json:"first_name"`
	LastName     string`json:"last_name"`
	Phone        string`json:"phone"`
	Active       string`json:"active"`
	Login        string`json:"login"`
	Password     string`json:"password"`
}

type UpdateAdmin struct {
	ID 	         string`json:"id"`
	FirstName    string`json:"first_name"`
	LastName     string`json:"last_name"`
	Phone        string`json:"phone"`
	Active       string`json:"active"`
}

type AdminsResponse struct {
	Admins []Admin  `json:"admins"`
	Count			int32`json:"count"`
}

type ChangeAdminPasswordRequest struct {
	Login       string`json:"login"`
	OldPassword string`json:"old_password"`
	NewPassword string`json:"new_password"`
}

type AdminHashedPasswordResponse struct {
	Password string`json:"password"`
}