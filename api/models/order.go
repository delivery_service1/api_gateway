package models

import (
	"time"
)

type Order struct {
	ID              string`json:"id"`
	ClientID       string`json:"client_id"`
	BranchID       string`json:"branch_id"`
	Type           string`json:"type"`
	Address        string`json:"address"`
	CourierID      string`json:"courier_id"`
	Price         float32`json:"price"`
	DeliveryPrice float32`json:"delivery_price"`
	Discount      float32`json:"discount"`
	OrderStatus    string`json:"order_status"`
	PaymentType    string`json:"payment_type"`
	CreatedAt   time.Time`json:"created_at"`
	UpdatedAt   time.Time`json:"updated_at"`
}

type CreateOrder struct { 
	ClientID       string`json:"client_id"`
	BranchID       string`json:"branch_id"`
	Type           string`json:"type"`
	Address        string`json:"address"`
	CourierID      string`json:"courier_id"`
	Price         float32`json:"price"`
	DeliveryPrice float32`json:"delivery_price"`
	Discount      float32`json:"discount"`
	OrderStatus    string`json:"order_status"`
	PaymentType    string`json:"payment_type"`
}

type UpdateOrder struct {
	ID              string`json:"id"`
	ClientID       string`json:"client_id"`
	BranchID       string`json:"branch_id"`
	Type           string`json:"type"`
	Address        string`json:"address"`
	CourierID      string`json:"courier_id"`
	Price         float32`json:"price"`
	DeliveryPrice float32`json:"delivery_price"`
	Discount      float32`json:"discount"`
	OrderStatus    string`json:"order_status"`
	PaymentType    string`json:"payment_type"`
}

type OrdersResponse struct {
	Orders []Order   `json:"orders"`
	Count			int32`json:"count"`
}

type UpdateOrderStatusRequest struct {
	ID 			string`json:"id"`
	OrderStatus string`json:"order_status"`
}