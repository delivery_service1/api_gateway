package models

import (
	"time"
)


type Courier struct {
	ID 	         	    string`json:"id"`
	FirstName    	    string`json:"first_name"`
	LastName     	    string`json:"last_name"`
	Phone        	    string`json:"phone"`
	Active				string`json:"active"`
	MaxOrderCount		 int32`json:"max_order_count"`
	BranchID            string`json:"branch_id"`
	CreatedAt 		 time.Time`json:"created_at"`
	UpdatedAt 		 time.Time`json:"updated_at"`
}

type CreateCourier struct {
	FirstName    	    string`json:"first_name"`
	LastName     	    string`json:"last_name"`
	Phone        	    string`json:"phone"`
	Active				string`json:"active"`
 	Login        	    string`json:"login"`
	Password     	    string`json:"password"`
	MaxOrderCount		 int32`json:"max_order_count"`
	BranchID            string`json:"branch_id"`
}

type UpdateCourier struct {
	ID 	         	    string`json:"id"`
	FirstName    	    string`json:"first_name"`
	LastName     	    string`json:"last_name"`
	Phone        	    string`json:"phone"`
	Active				string`json:"active"`
	MaxOrderCount		 int32`json:"max_order_count"`
	BranchID            string`json:"branch_id"`
}

type CouriersResponse struct {
	Couriers []Courier  `json:"couriers"`
	Count			int32`json:"count"`
}

type ChangeCourierPasswordRequest struct {
	Login       string`json:"login"`
	OldPassword string`json:"old_password"`
	NewPassword string`json:"new_password"`
}

type CourierHashedPasswordResponse struct {
	Password string`json:"password"`
}