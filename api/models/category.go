package models

import (
	"time"
)

type Category struct {
	ID 			 string`json:"id"`
	Title 		 string`json:"title"`
	Active 		 string`json:"active"`
	CreatedAt time.Time`json:"created_at"`
	UpdatedAt time.Time`json:"updated_at"`
}

type CreateCategory struct {
	Title 		 string`json:"title"`
	Active 		 string`json:"active"`
}

type UpdateCategory struct {
	ID 			 string`json:"id"`
	Title 		 string`json:"title"`
	Active 		 string`json:"active"`
}

type CategoriesResponse struct {
	Categories []Category`json:"categories"`
	Count			int32`json:"count"`
}