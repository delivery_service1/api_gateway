package models

import (
	"time"
)
 
type Branch struct {
	ID 	         	string`json:"id"`
	Name 			string`json:"name"`
	Phone 			string`json:"phone"`
	DeliveryTarifID string`json:"delivery_tarif_id"`
	WorkHourStart 	string`json:"work_hour_start"`
	WorkHourEnd 	string`json:"work_hour_end"`
	Address 		string`json:"address"`
	Destination 	string`json:"destination"`
	Active 			string`json:"active"`
	CreatedAt 		time.Time`json:"created_at"`
	UpdatedAt 		time.Time`json:"updated_at"`
}

type CreateBranch struct {
	Name 			string`json:"name"`
	Phone 			string`json:"phone"`
	DeliveryTarifID string`json:"delivery_tarif_id"`
	WorkHourStart 	string`json:"work_hour_start"`
	WorkHourEnd 	string`json:"work_hour_end"`
	Address 		string`json:"address"`
	Destination 	string`json:"destination"`
	Active 			string`json:"active"`
}

type UpdateBranch struct {
	ID 	         	string`json:"id"`
	Name 			string`json:"name"`
	Phone 			string`json:"phone"`
	DeliveryTarifID string`json:"delivery_tarif_id"`
	WorkHourStart 	string`json:"work_hour_start"`
	WorkHourEnd 	string`json:"work_hour_end"`
	Address 		string`json:"address"`
	Destination 	string`json:"destination"`
	Active 			string`json:"active"`
}

type BranchesResponse struct {
	Branches []Branch  `json:"branches"`
	Count			int32`json:"count"`
}