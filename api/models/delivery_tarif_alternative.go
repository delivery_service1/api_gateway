package models

import (
	"time"
)

type DeliveryTarifAlternative struct {
	ID           string`json:"id"`
    Type         string`json:"type"`
	CreatedAt time.Time`json:"created_at"`
	UpdatedAt time.Time`json:"updated_at"`
}

type CreateDeliveryTarifAlternative struct { 
    Type         string`json:"type"`
}

type UpdateDeliveryTarifAlternative struct {
	ID           string`json:"id"`
    Type         string`json:"type"`
}

type DeliveryTarifAlternativesResponse struct {
	DeliveryTarifAlternatives []DeliveryTarifAlternative   `json:"delivery_tarif_alternatives"`
	Count			int32`json:"count"`
}

 