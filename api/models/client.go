package models

import (
	"time"
)

type Client struct {
	ID 	         	    string`json:"id"`
	FirstName    	    string`json:"first_name"`
	LastName     	    string`json:"last_name"`
	Phone        	    string`json:"phone"`
	DateOfBirth  	 time.Time`json:"date_of_birth"`
	LastOrderedDate  time.Time`json:"last_ordered_date"`
	TotalOrdersSum     float32`json:"total_orders_sum"`
	TotalOrdersCount     int32`json:"total_orders_count"`
	DiscountType        string`json:"discount_type"`
	DisocuntAmount     float32`json:"discount_amount"`
	CreatedAt 		 time.Time`json:"created_at"`
	UpdatedAt 		 time.Time`json:"updated_at"`
}

type CreateClient struct {
	FirstName    	    string`json:"first_name"`
	LastName     	    string`json:"last_name"`
	Phone        	    string`json:"phone"`
 	Login        	    string`json:"login"`
	Password     	    string`json:"password"`
	DateOfBirth  	 time.Time`json:"date_of_birth"`
	LastOrderedDate  time.Time`json:"last_ordered_date"`
	TotalOrdersSum     float32`json:"total_orders_sum"`
	TotalOrdersCount     int32`json:"total_orders_count"`
	DiscountType        string`json:"discount_type"`
	DisocuntAmount     float32`json:"discount_amount"`
}

type UpdateClient struct {
	ID 	         	    string`json:"id"`
	FirstName    	    string`json:"first_name"`
	LastName     	    string`json:"last_name"`
	Phone        	    string`json:"phone"`
 	DateOfBirth  	 time.Time`json:"date_of_birth"`
	LastOrderedDate  time.Time`json:"last_ordered_date"`
	TotalOrdersSum     float32`json:"total_orders_sum"`
	TotalOrdersCount     int32`json:"total_orders_count"`
	DiscountType        string`json:"discount_type"`
	DisocuntAmount     float32`json:"discount_amount"`
}

type ClientsResponse struct {
	Clients []Client  `json:"clients"`
	Count			int32`json:"count"`
}

type ChangeClientPasswordRequest struct {
	Login       string`json:"login"`
	OldPassword string`json:"old_password"`
	NewPassword string`json:"new_password"`
}

type ClientHashedPasswordResponse struct {
	Password string`json:"password"`
}