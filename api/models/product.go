package models

import (
	"time"
)

type Product struct {
	ID 			 string`json:"id"`
	Description  string`json:"description"`
	OrderNumber   int32`json:"order_number"`
	Active 		 string`json:"active"`
	Type		 string`json:"type"`
	Price		float32`json:"price"`
	CategoryID   string`json:"category_id"`
	CreatedAt time.Time`json:"created_at"`
	UpdatedAt time.Time`json:"updated_at"`
}

type CreateProduct struct { 
	Description  string`json:"description"`
	OrderNumber   int32`json:"order_number"`
	Active 		 string`json:"active"`
	Type		 string`json:"type"`
	Price		float32`json:"price"`
	CategoryID   string`json:"category_id"`
}

type UpdateProduct struct {
	ID 			 string`json:"id"`
	Description  string`json:"description"`
	OrderNumber   int32`json:"order_number"`
	Active 		 string`json:"active"`
	Type		 string`json:"type"`
	Price		float32`json:"price"`
	CategoryID   string`json:"category_id"`
}

type ProductsResponse struct {
	Products []Product   `json:"products"`
	Count			int32`json:"count"`
}