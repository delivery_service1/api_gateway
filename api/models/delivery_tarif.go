package models

import (
	"time"
)

type DeliveryTarif struct {
	ID           			   string`json:"id"`
	Name         			   string`json:"name"`
	Type       				   string`json:"type"`
	BasePrice   			  float32`json:"base_price"`
	DeliveryTarifAlternativeID string`json:"delivery_tarif_alternative_id"`
	CreatedAt 				time.Time`json:"created_at"`
	UpdatedAt 				time.Time`json:"updated_at"`
}

type CreateDeliveryTarif struct { 
	Name         			   string`json:"name"`
	Type       				   string`json:"type"`
	BasePrice   			  float32`json:"base_price"`
	DeliveryTarifAlternativeID string`json:"delivery_tarif_alternative_id"`
}

type UpdateDeliveryTarif struct {
	ID           			   string`json:"id"`
	Name         			   string`json:"name"`
	Type       				   string`json:"type"`
	BasePrice   			  float32`json:"base_price"`
	DeliveryTarifAlternativeID string`json:"delivery_tarif_alternative_id"`
}

type DeliveryTarifsResponse struct {
	DeliveryTarifs []DeliveryTarif   `json:"delivery_tarifs"`
	Count			int32`json:"count"`
}

 