package api

import (
	"github.com/gin-gonic/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "api_gateway/api/docs"
	"api_gateway/api/handler"
)

// New ...
// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(h handler.Handler) *gin.Engine {
	r := gin.New()

		// Catalog service

		r.POST("/category", h.CreateCategory)
		r.GET("/category/:id", h.GetCategory)
		r.GET("/categories", h.GetCategoryList)
		r.PUT("/category/:id",h.UpdateCategory)
		r.DELETE("/category/:id",h.DeleteCategory)


		r.POST("/product", h.CreateProduct)
		r.GET("/product/:id", h.GetProduct)
		r.GET("/products", h.GetProductList)
		r.PUT("/product/:id",h.UpdateProduct)
		r.DELETE("/product/:id",h.DeleteProduct)

		// User service

		r.POST("/admin", h.CreateAdmin)
		r.GET("/admin/:id", h.GetAdmin)
		r.GET("/admins", h.GetAdminList)
		r.PUT("/admin/:id",h.UpdateAdmin)
		r.DELETE("/admin/:id",h.DeleteAdmin)
		r.PUT("/admin_password", h.UpdateAdminPassword)

		r.POST("/branch", h.CreateBranch)
		r.GET("/branch/:id", h.GetBranch)
		r.GET("/branches", h.GetBranchList)
		r.GET("/active_branches", h.GetActiveBranchesList)
		r.PUT("/branch/:id",h.UpdateBranch)
		r.DELETE("/branch/:id",h.DeleteBranch)

		r.POST("/client", h.CreateClient)
		r.GET("/client/:id", h.GetClient)
		r.GET("/clients", h.GetClientList)
		r.PUT("/client/:id",h.UpdateClient)
		r.DELETE("/client/:id",h.DeleteClient)
		r.PUT("/client_password", h.UpdateClientPassword)


		r.POST("/courier", h.CreateCourier)
		r.GET("/courier/:id", h.GetCourier)
		r.GET("/couriers", h.GetCourierList)
		r.PUT("/courier/:id",h.UpdateCourier)
		r.DELETE("/courier/:id",h.DeleteCourier)
		r.PUT("/courier_password", h.UpdateCourierPassword)

		r.POST("/client_login", h.ClientLogin)
		r.POST("/admin_login", h.AdminLogin)
		r.POST("/courier_login", h.CourierLogin)


		// Order service

		r.POST("/order", h.CreateOrder)
		r.GET("/order/:id", h.GetOrder)
		r.GET("/orders", h.GetOrderList)
		r.PUT("/order/:id",h.UpdateOrder)
		r.DELETE("/order/:id",h.DeleteOrder)
		r.PATCH("/order_status",h.UpdateOrderStatus)

		r.POST("/order_product", h.CreateOrderProduct)
		r.GET("/order_product/:id", h.GetOrderProduct)
		r.GET("/order_products", h.GetOrderProductList)
		r.PUT("/order_product/:id",h.UpdateOrderProduct)
		r.DELETE("/order_product/:id",h.DeleteOrderProduct)

		r.POST("/delivery_tarif_alternative", h.CreateDeliveryTarifAlternative)
		r.GET("/delivery_tarif_alternative/:id", h.GetDeliveryTarifAlternative)
		r.GET("/delivery_tarif_alternatives", h.GetDeliveryTarifAlternativeList)
		r.PUT("/delivery_tarif_alternative/:id",h.UpdateDeliveryTarifAlternative)
		r.DELETE("/delivery_tarif_alternative/:id",h.DeleteDeliveryTarifAlternative)

		r.POST("/delivery_tarif", h.CreateDeliveryTarif)
		r.GET("/delivery_tarif/:id", h.GetDeliveryTarif)
		r.GET("/delivery_tarifs", h.GetDeliveryTarifList)
		r.PUT("/delivery_tarif/:id",h.UpdateDeliveryTarif)
		r.DELETE("/delivery_tarif/:id",h.DeleteDeliveryTarif)
	
		r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
		return r
}
