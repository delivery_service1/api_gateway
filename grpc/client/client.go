package client

import (
	"api_gateway/config"
	pbc"api_gateway/genproto/catalog_protos"
	pbu"api_gateway/genproto/user_protos"
	pbo"api_gateway/genproto/order_protos"
 

	"fmt"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	// Catalog service

	CategoryService() pbc.CategoryServiceClient
	ProductService() pbc.ProductServiceClient

	// User service

	AdminService() pbu.AdminServiceClient
	BranchService() pbu.BranchServiceClient
	ClientService() pbu.ClientServiceClient
	CourierService() pbu.CourierServiceClient

	AuthService() pbu.AuthServiceClient

	// OrderService

	OrderService() pbo.OrderServiceClient
	OrderProductService() pbo.OrderProductServiceClient
	DeliveryTarifAlternativeService() pbo.DeliveryTarifAlternativeServiceClient
	DeliveryTarifService() pbo.DeliveryTarifServiceClient

}

type grpcClients struct {
	// Catalog service

	categoryService pbc.CategoryServiceClient
	productService pbc.ProductServiceClient

	// User service

	adminService pbu.AdminServiceClient
	branchService pbu.BranchServiceClient
	clientService pbu.ClientServiceClient
	courierService pbu.CourierServiceClient

	authService pbu.AuthServiceClient

	// Order service 

	orderService pbo.OrderServiceClient
	orderProductService pbo.OrderProductServiceClient
	deliveryTarifAlternativeService pbo.DeliveryTarifAlternativeServiceClient
	deliveryTarifService pbo.DeliveryTarifServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connCatalogService, err := grpc.Dial(
		cfg.CatalogGrpcServiceHost+cfg.CatalogGrpcServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		fmt.Println("Error while dialing with catalog service client",err.Error())
		return nil, err
	}

	connUserService, err := grpc.Dial(
		cfg.UserGrpcServiceHost+cfg.UserGrpcServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		fmt.Println("Error while dialing with user service client",err.Error())
		return nil, err
	}

	connOrderService, err := grpc.Dial(
		cfg.OrderGrpcServiceHost+cfg.OrderGrpcServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		fmt.Println("Error while dialing with order service client",err.Error())
		return nil, err
	}

	return &grpcClients{
		categoryService: pbc.NewCategoryServiceClient(connCatalogService),
		productService: pbc.NewProductServiceClient(connCatalogService),

		adminService: pbu.NewAdminServiceClient(connUserService),
		branchService: pbu.NewBranchServiceClient(connUserService),
		clientService: pbu.NewClientServiceClient(connUserService),
		courierService: pbu.NewCourierServiceClient(connUserService),

		authService: pbu.NewAuthServiceClient(connUserService),

		orderService: pbo.NewOrderServiceClient(connOrderService),
		orderProductService: pbo.NewOrderProductServiceClient(connOrderService),
		deliveryTarifAlternativeService: pbo.NewDeliveryTarifAlternativeServiceClient(connOrderService),
		deliveryTarifService: pbo.NewDeliveryTarifServiceClient(connOrderService),
		


	}, nil
}

// Catalog service

func (g *grpcClients) CategoryService() pbc.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() pbc.ProductServiceClient{
	return g.productService
}

// User service

func (g *grpcClients) AdminService() pbu.AdminServiceClient{
	return g.adminService
}

func (g *grpcClients) BranchService() pbu.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) ClientService() pbu.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) CourierService() pbu.CourierServiceClient {
	return g.courierService
}

func (g *grpcClients) AuthService() pbu.AuthServiceClient {
	return g.authService
}

// Order service

func (g *grpcClients) OrderService() pbo.OrderServiceClient{
	return g.orderService
}

func (g *grpcClients) OrderProductService() pbo.OrderProductServiceClient {
	return g.orderProductService
}

func (g *grpcClients) DeliveryTarifAlternativeService() pbo.DeliveryTarifAlternativeServiceClient {
	return g.deliveryTarifAlternativeService
}

func (g *grpcClients) DeliveryTarifService() pbo.DeliveryTarifServiceClient {
	return g.deliveryTarifService
}
