package main

import (
	"api_gateway/api"
	"api_gateway/api/handler"
	"api_gateway/config"
	"api_gateway/grpc/client"
	"fmt"
)

func main() {
	cfg := config.Load()

	services, err := client.NewGrpcClients(cfg)
	if err != nil {
		fmt.Println("Error while initializing grpc clients",err.Error())
		return
	}

	h := handler.New(cfg, services)

	r := api.New(h)

	fmt.Println("Server is running ...", "port", cfg.HTTPPort)
	err = r.Run(cfg.HTTPPort)
	if err != nil {
		fmt.Println("Error while running server", err.Error())
	}
}
